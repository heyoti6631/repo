#include <Windows.h>
#include <conio.h>
#include <iostream>
#include <iostream>
#include <fstream>

#include "LexAnl.h"

enum Type
{
	eT_Identifier,
	eT_Operator,
	eT_Separator,
	eT_Keyword,
	eT_Statement,
	eT_Literal,
	eT_Incorrect
};

std::vector<std::string> vec_name_lex =
{
	 "eT_Identifier",
	 "eT_Operator",
	 "eT_Separator",
	 "eT_Keyword",
	 "eT_Statement",
	 "eT_Literal",
	 "eT_Incorrect"
};

struct sLex
{
	Type        type;
	std::string value;
};

bool operator==(const sLex& a1, const sLex& a2)
{
	return (a1.type == a2.type && a1.value == a2.value);
}

void CorrectData(const std::string& buffer, std::vector<sLex>& data)
{
	if (is_operator(buffer))
		data.push_back({ eT_Operator , buffer });
	else if (is_keyword(buffer))
		data.push_back({ eT_Keyword , buffer });
	else if (is_statement(buffer))
		data.push_back({ eT_Statement , buffer });
	else if (isLiteral(buffer))
		data.push_back({ eT_Literal , buffer });
	else if (isID(buffer))
		data.push_back({ eT_Identifier , buffer });
	else if (is_separator(buffer))
		data.push_back({ eT_Separator , buffer });
	else
		data.push_back({ eT_Incorrect , "Incorrect" });
}

std::vector<sLex>  vAnalyze(const std::string& szStr)
{
	std::vector<sLex> data = {};
	std::string buffer;

	for (size_t i = 0; i < szStr.size(); i++)
	{
		char ch = szStr[i];
		if (isNotLegal(std::string(1, ch)))
		{
			if (!buffer.empty())
			{
				CorrectData(buffer, data);
				buffer = "";
			}
			continue;
		}

		if (is_operator(std::string(1, ch)) && !is_operator(buffer))
		{
			if (!buffer.empty())
			{
				CorrectData(buffer, data);
				buffer = "";
			}
		}

		if (!is_operator(std::string(1, ch)) && is_operator(buffer))
		{
			CorrectData(buffer, data);
			buffer = "";
		}

		if (is_separator(std::string(1, ch)))
		{
			if (!buffer.empty())
			{
				CorrectData(buffer, data);
				buffer = "";
			}
			if (is_separator(std::string(1, ch)))
			{
				CorrectData(std::string(1, ch), data);
				continue;
			}
		}

		buffer += ch;
	}
	return data;
}


void TestCorrrect(std::vector<sLex>  data, std::vector<sLex>  check)
{
	if (data == check)
		std::cout << "  Corrrect data" << std::endl;
	else
		std::cout << "  InCorrrect data" << std::endl;
}

void Call_1()
{
	std::string szStr = "const int num = 23;";
	std::vector<sLex> lex = vAnalyze(szStr);

	for (const auto v : lex)
		std::cout << "{" << vec_name_lex[v.type] << ", " << "\"" << v.value << "\"" << "}," << std::endl;

	std::vector<sLex>  check =
	{
		{eT_Identifier, "const" },
		{eT_Keyword,    "int" },
		{eT_Identifier, "num" },
		{eT_Operator,   "=" },
		{eT_Literal,    "23" },
		{eT_Separator,  ";" }
	};

	std::vector<sLex>  check2 =
	{
		{eT_Keyword,    "int" },
		{eT_Identifier, "num" },
		{eT_Operator,   "=" },
		{eT_Literal,    "23" },
		{eT_Separator,  ";" }
	};

	std::cout << "\nCorrrect Test:" << std::endl;
	TestCorrrect(lex, check);

	std::cout << "InCorrrect Test:" << std::endl;
	TestCorrrect(lex, check2);
}

void Call_2()
{
	std::string szStr =
		"int x = 5;" \
		"int y = 0;" \
		"for(;x < 10;x++)" \
		"{" \
		"y +=x*4;" \
		"}";

	std::vector<sLex> lex = vAnalyze(szStr);

	for (const auto v : lex)
		std::cout << "{" << vec_name_lex[v.type] << ", " << "\"" << v.value << "\"" << "}," << std::endl;

	std::vector<sLex>  valid =
	{
		{ eT_Keyword, "int"},
		{ eT_Identifier, "x" },
		{ eT_Operator, "=" },
		{ eT_Literal, "5" },
		{ eT_Separator, ";" },
		{ eT_Keyword, "int" },
		{ eT_Identifier, "y" },
		{ eT_Operator, "=" },
		{ eT_Literal, "0" },
		{ eT_Separator, ";" },
		{ eT_Statement, "for" },
		{ eT_Separator, "(" },
		{ eT_Separator, ";" },
		{ eT_Identifier, "x" },
		{ eT_Operator, "<" },
		{ eT_Literal, "10" },
		{ eT_Separator, ";" },
		{ eT_Identifier, "x" },
		{ eT_Operator, "++" },
		{ eT_Separator, ")" },
		{ eT_Separator, "{" },
		{ eT_Identifier, "y" },
		{ eT_Operator, "+=" },
		{ eT_Identifier, "x" },
		{ eT_Operator, "*" },
		{ eT_Literal, "4" },
		{ eT_Separator, ";" },
		{ eT_Separator, "}" }
	};

	std::vector<sLex>  incorrect =
	{
		{ eT_Keyword, "int"},
		{ eT_Identifier, "z" },
		{ eT_Operator, "=" },
		{ eT_Literal, "6" },
		{ eT_Separator, ";" },
		{ eT_Keyword, "int" },
		{ eT_Identifier, "y" },
		{ eT_Operator, "=" },
		{ eT_Literal, "0" },
		{ eT_Separator, ";" },
		{ eT_Statement, "for" },
		{ eT_Separator, "(" },
		{ eT_Separator, ";" },
		{ eT_Identifier, "z" },
		{ eT_Operator, "<" },
		{ eT_Literal, "10" },
		{ eT_Separator, ";" },
		{ eT_Identifier, "z" },
		{ eT_Operator, "++" },
		{ eT_Separator, ")" },
		{ eT_Separator, "{" },
		{ eT_Identifier, "y" },
		{ eT_Operator, "+=" },
		{ eT_Identifier, "z" },
		{ eT_Operator, "+" },
		{ eT_Literal, "7" },
		{ eT_Separator, ";" },
		{ eT_Separator, "}" }
	};

	std::cout << "\nCorrrect Test:" << std::endl;
	TestCorrrect(lex, valid);

	std::cout << "InCorrrect Test:" << std::endl;
	TestCorrrect(lex, incorrect);
}

int main(int argc, char** argv)
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	SetConsoleTitleA("LexicalAnalyzer");

	Call_1();
	std::cout << "\n\n\n";
	Call_2();

	return _getch();
}
