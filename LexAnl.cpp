#include "LexAnl.h"

const std::vector<std::string> vec_separators
{
    "(",
    ")",
    "{", 
    "}", 
    ",", 
    ";"
};
const std::vector<std::string> vec_operators
{ 
    "=="
    "/=",
    "<=", 
    ">=",
    "-=",
    "*=",
    "+=",
    "++",
    "--",
    "<",
    ">",
    "*", 
    "+", 
    "-", 
    "/", 
    "=", 
};
const std::vector<std::string> vec_statements
{ 
    "while"
    "for", 
};
const std::vector<std::string> vec_keywords
{ 
    "auto",
    "int", 
    "float", 
    "double", 
    "do", 
    "switch", 
    "return" 
};

bool is_separator(const std::string& str)
{
    for (const auto& v : vec_separators)
        if (v == str)
            return true;
    return false;
}

bool is_operator(const std::string& str)
{
    for (const auto& v : vec_operators)
        if (v == str)
            return true;
    return false;
}

bool is_statement(const std::string& str)
{
    for (const auto& v : vec_statements)
        if (v == str)
            return true;
    return false;
}

bool is_keyword(const std::string& str)
{
    for (const auto& v : vec_keywords)
        if (v == str)
            return true;

    return false;
}

bool is_digit(const std::string& str)
{
    return std::all_of(str.begin(), str.end(), ::isdigit);
}

bool is_string(const std::string& str)
{
    return str[0] == '"' && str[str.size() - 1] == '"';
}

bool is_bool(const std::string& str)
{
    return str == "true" || str == "false";
}

bool isID(const std::string& str)
{
    if (std::isdigit(str[0]))
        return false;

    size_t counter = 0;

    if (str[0] == '_')
        counter++;

    for (; counter < str.size(); counter++)
        if (!isalnum(str[counter]))
            return false;

    return true;
}

bool isNotLegal(const std::string& str)
{
    return str == " " || str == "\n";
}

bool isLiteral(const std::string& str)
{
    return is_digit(str) || is_string(str) || is_bool(str);
}