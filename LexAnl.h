#pragma once

#include <string>
#include <vector>
#include <algorithm>


bool is_separator(const std::string& str);
bool is_operator(const std::string& str);
bool is_statement(const std::string& str);
bool is_keyword(const std::string& str);
bool is_digit(const std::string& str);
bool is_string(const std::string& str);
bool is_bool(const std::string& str);
bool isID(const std::string& str);
bool isLiteral(const std::string& str);
bool isNotLegal(const std::string& str);